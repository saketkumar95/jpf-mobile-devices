package jp.ac.chiba_u.s.math.jpf;

import java.io.File;
import java.util.List;

/**
 * An utility class for helper functions.
 */
public final class Utils {
  private Utils() {}

  static String listToString(List<String> list, String sep) {
    StringBuilder s = new StringBuilder();

    for (int i = 0; i < list.size() - 1; i++) {
      s.append(list.get(i));
      s.append(sep);
    }

    s.append(list.get(list.size() - 1));

    return s.toString();
  }
}
