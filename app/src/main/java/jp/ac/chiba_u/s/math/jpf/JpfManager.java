package jp.ac.chiba_u.s.math.jpf;

import android.content.Context;
import android.content.Intent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Starts jpf-core.
 * Also, updates configuration files of jpf-core.
 */
public class JpfManager {

  /**
   * Starts execution of underlying jpf-core.
   *
   * @param context Android context to launch the service
   * @param argLine jpf-core arguments
   */
  public static void startJPF(Context context, String argLine) {
    // get args
    String[] args = argLine.isEmpty() ? new String[]{} : argLine.split(" +");

    // replace dot in the beginning of a path for convenience
    // TODO: make sure that replacement does not broke other parameters
    String homeDir = Relay.getProperty("user.home");
    String sep = Relay.getProperty("file.separator");

    for (int i = 0; i < args.length; i++) {
      if (args[i].startsWith("./") && args[i].length() > 2) {
        args[i] = homeDir + sep + args[i].substring(2, args[i].length());
      }
    }

    // a background thread to pass messages from jpf-core to main Activity
    new MessagePollThread().execute();

    // start jpf-core service
    Intent intent = JpfCoreService.newIntent(context, args);
    context.startService(intent);
  }

  private static String getSitePropertiesPath() {
    String sep = Relay.getProperty("file.separator");
    return Relay.getProperty("user.home") + sep + "jpf" + sep + "site.properties";
  }

  static void setExtensionList(List<ExtensionDescriptor> extList) throws IOException {
    final String SITE_PATH = getSitePropertiesPath();
    final String EXT_LINE = "extensions=";

    // prepare extension list
    List<String> extNameList = new ArrayList<>();
    for (ExtensionDescriptor d : extList) {
      if (d.isEnabled()) {
        extNameList.add(String.format("${%s}", d.getName()));
      }
    }
    String extString = EXT_LINE + Utils.listToString(extNameList, ",");

    // read contents of site.properties
    String contents = new String(IOUtils.read(new FileInputStream(new File(SITE_PATH))));

    // update jpf extension list in the file
    int extLineStart = contents.indexOf(EXT_LINE);
    int extLineEnd = contents.indexOf("\n", extLineStart);
    String result = contents.substring(0, extLineStart) + extString + contents.substring(extLineEnd, contents.length());

    IOUtils.write(result.getBytes(), SITE_PATH);
  }
}
