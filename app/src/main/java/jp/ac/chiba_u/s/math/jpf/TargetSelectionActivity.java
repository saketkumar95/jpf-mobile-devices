package jp.ac.chiba_u.s.math.jpf;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class TargetSelectionActivity extends AppCompatActivity {
  private Stack<String> visitedDirectories;
  private List<File> fileList = new ArrayList<>();
  private String parentDir = null;

  private Button backButton;
  private Button upButton;
  private FileListAdapter fileListAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_target_selection); // init UI

    // init directory stack
    visitedDirectories = new Stack<>();
    visitedDirectories.push(Settings.EXAMPLES_DIR);

    // init RecyclerView
    RecyclerView listView = (RecyclerView) findViewById(R.id.target_selection_list);
    listView.setLayoutManager(new LinearLayoutManager(this)); // RecyclerView requires a layout manager
    fileListAdapter = new FileListAdapter(fileList);
    listView.setAdapter(fileListAdapter);

    // init buttons

    backButton = (Button) findViewById(R.id.target_selection_button_back);
    backButton.setOnClickListener(view -> {
      if (visitedDirectories.size() > 1) {
        visitedDirectories.pop();
        scanCurrentDir();
      }
    });

    Button homeButton = (Button) findViewById(R.id.target_selection_button_home);
    homeButton.setOnClickListener(view -> {
      if (!visitedDirectories.peek().equals(Settings.EXAMPLES_DIR)) {
        visitedDirectories.push(Settings.EXAMPLES_DIR);
        scanCurrentDir();
      }
    });

    upButton = (Button) findViewById(R.id.target_selection_button_up);
    upButton.setOnClickListener(view -> {
      visitedDirectories.push(parentDir);
      scanCurrentDir();
    });

    scanCurrentDir(); // loads list of JPF files of the current dir
  }

  private void scanCurrentDir() {
    // disable buttons to prevent user actions when data is still loading
    disableButtons();

    fileList.clear();

    String currentDir = visitedDirectories.peek();
    List<File> files = IOUtils.getFiles(currentDir, "jpf");

    if (files != null) {
      fileList.addAll(files);
      parentDir = IOUtils.getParentDir(currentDir);
    } else {
      parentDir = null;
    }

    fileListAdapter.notifyDataSetChanged();

    // re-enable buttons
    backButton.setEnabled(visitedDirectories.size() > 1);
    upButton.setEnabled(parentDir != null);
  }


  private void disableButtons() {
    backButton.setEnabled(false);
    upButton.setEnabled(false);
  }


  private class FileListAdapter extends RecyclerView.Adapter<FileItemHolder> {
    private List<File> fileList;

    private FileListAdapter(List<File> fileList) {
      this.fileList = fileList;
    }

    @Override
    public FileItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = getLayoutInflater().inflate(R.layout.list_item_file_holder, parent, false);
      return new FileItemHolder(view);
    }

    @Override
    public void onBindViewHolder(FileItemHolder itemHolder, int position) {
      File file = fileList.get(position);
      itemHolder.bind(file);
    }

    @Override
    public int getItemCount() {
      return fileList.size();
    }
  }

  private class FileItemHolder extends RecyclerView.ViewHolder {
    private File file;

    private ImageView fileTypeImage;
    private TextView fileNameField;

    public FileItemHolder(View itemView) {
      super(itemView);

      // init UI elements
      fileTypeImage = (ImageView) itemView.findViewById(R.id.files_item_type_img);
      fileNameField = (TextView) itemView.findViewById(R.id.files_item_file_name);

      // click on a file -> return the file name
      fileNameField.setOnLongClickListener(view -> {

        String path = file.getAbsolutePath();

        if (file.isFile()) {
          Settings.getInstance().setTarget(path);
          setResult(RESULT_OK);
          finish();
          return true;
        } else {
          visitedDirectories.push(path);
          scanCurrentDir();
        }

        return false;
      });
    }

    // model -> view
    public void bind(File f) {
      file = f;

      if (f.isDirectory())
        fileTypeImage.setImageResource(R.drawable.file_type_dir);
      else
        fileTypeImage.setImageResource(R.drawable.file_type_file);

      fileNameField.setText(f.getName());
    }
  }
}
