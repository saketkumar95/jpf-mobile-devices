package jp.ac.chiba_u.s.math.jpf;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Periodically checks output message queue for new messages
 * and sends a display request to the main UI thread
 */
public class MessagePollThread extends AsyncTask<Void, Void, Void> {
  private static final int POLL_INTERVAL_MSEC = 50;

  private static boolean sEnabled = false;
  private static final List<String> sQueue = new ArrayList<>();
  private static final StringBuilder sBuilder = new StringBuilder();
  private static String oneLineBuffer = "";

  @Override
  protected Void doInBackground(Void... params) {
    // enable the cycle
    sEnabled = true;

    while (sEnabled) {
      try {
        // check message queue
        processQueue();

        // sleep
        Thread.sleep(POLL_INTERVAL_MSEC);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  private static void processQueue() {
    boolean oneLine;

    synchronized (sQueue) {
      int size = sQueue.size();

      switch (size) {
        case 0:
          return;
        case 1:
          oneLine = true;
          oneLineBuffer = sQueue.get(0);
          sQueue.clear();
          break;
        default:
          oneLine = false;
          sBuilder.setLength(0);

          for (String msg : sQueue) {
            sBuilder.append(msg);
          }

          sQueue.clear();
          break;
      }
    }

    display(oneLine ? oneLineBuffer : sBuilder.toString());
  }

  public static void enable(boolean state) {
    sEnabled = state;

    if (!state) {
      // if we suddenly stop the thread, we need to process remaining messages
      processQueue();
    }
  }

  private static void display(String buffer) {
    MainActivity.sendMessage(buffer);
  }

  public static void add(String msg) {
    if (sEnabled) {
      synchronized (sQueue) {
        sQueue.add(msg);
      }
    } else {
      display(msg);
    }
  }
}
