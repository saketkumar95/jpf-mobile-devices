package jp.ac.chiba_u.s.math.jpf.jvm;

import java.io.File;
import java.io.IOException;

import gov.nasa.jpf.jvm.JVMSystemClassLoaderInfo;
import gov.nasa.jpf.jvm.JarClassFileContainer;
import gov.nasa.jpf.vm.ClassFileContainer;
import gov.nasa.jpf.vm.VM;

/**
 * Added to support loading Java libraries in Android format (.dex, .odex).
 */
public class AndroidVMSystemClassLoaderInfo extends JVMSystemClassLoaderInfo {

  public AndroidVMSystemClassLoaderInfo(VM vm, int appId) {
    super(vm, appId);
  }

  @Override
  protected ClassFileContainer createClassFileContainer(String spec) {
    ClassFileContainer container = null;

    if (spec.endsWith(".jar")) {
      File jarFile = new File(spec);

      if (jarFile.isFile()) {
        try {
          container = new DexClassFileContainer(jarFile);
        } catch (IOException e) {
          container = null;
        }
      }
    }

    return container;
  }
}
