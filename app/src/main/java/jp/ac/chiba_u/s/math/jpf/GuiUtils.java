package jp.ac.chiba_u.s.math.jpf;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Contains utility functions for UI / UX.
 */
final class GuiUtils {
  private GuiUtils() {
  }

  static void closeSoftKeyboard(Activity activity) {
    InputMethodManager inputManager = (InputMethodManager)
        activity.getSystemService(Context.INPUT_METHOD_SERVICE);

    View view = activity.getCurrentFocus();

    if (view != null)
      inputManager.hideSoftInputFromWindow(view.getWindowToken(),
          InputMethodManager.HIDE_NOT_ALWAYS);
  }
}
