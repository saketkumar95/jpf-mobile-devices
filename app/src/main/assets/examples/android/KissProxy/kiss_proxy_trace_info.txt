﻿KissProxy sample execution (launch -> enable -> disable -> exit)
* = event, no mark = regular method

------------ launch ------------
 1. * Settings.onCreate
 2. * ProxyService.onCreate
 3. * ProxyService.onBind
 4. * Settings.onServiceConnected
 5.   Settings.updateProxy
------------ enable service ------------
 6. * Settings.onCheckedChanged
 7.   Settings.updateProxy
 8.   Settings.startProxy
 9.   ProxyService.doStart
------------ disable service ------------
10. * Settings.onCheckedChanged
11.   Settings.updateProxy
12.   Settings.stopProxy
13.   ProxyService.doStop
------------ exit ------------
(no methods are called on exit)

